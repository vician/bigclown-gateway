#!/bin/bash

led1="/sys/class/leds/omnia-led:user1/color"
led2="/sys/class/leds/omnia-led:user2/color"

function led {
	color=$1
	echo "$color" | tee $led1 $led2
	sleep 1
}

# white
led "255 255 255"
# blue
led "0 0 255"
# orange
led "255 255 0"
# orange red
led "255 0 127"
# red
led "255 0 0"
